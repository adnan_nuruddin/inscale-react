import React from 'react';
import './index.css';
import GithubLogo from '../../github.png';

export default () => {
	return (
		<div className="page-footer">
			<div className="footer-container">
				<span>Adnan Nuruddin © 2019</span>
				<span className="pull-right">GitHub</span>
				<img className="github-logo pull-right" src={GithubLogo} alt="GitHub" />
			</div>
		</div>
	)
}